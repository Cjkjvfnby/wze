from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.utils.functional import SimpleLazyObject

admin = User.objects.filter(is_superuser=True)[0]

class UserMiddleware(object):
    def process_request(self, request):
        request.user = admin


