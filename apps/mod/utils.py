import os
from zipfile import ZipFile
from itertools import izip_longest
from django.conf import settings


def get_file(file_name):
    path = settings.PATH_TO_MOD
    if settings.MOD_SOURCE:
        text = get_file_from_folder(path, file_name)
        lines = [line for line in text.replace('\r', '').split('\n') if line][1:]
    else:
        text = get_file_from_zip(path, file_name)
        lines = [line for line in text.replace('\r', '').split('\n') if line]
    return [x.split(',') for x in lines]


def get_file_from_folder(path, file_name):
    file_path = os.path.join(path, file_name)
    if os.path.exists(file_path):
        return open(file_path).read()


def get_file_from_zip(path, file_name):
    zf = ZipFile(path)
    assert file_name in zf.namelist(), 'Filename is not present in archive %s' % file_name
    return zf.read(file_name)


def bulk_save(item_list, num):
    '''
    sqlite limitation hack
    https://docs.djangoproject.com/en/dev/ref/models/querysets/#django.db.models.query.QuerySet.bulk_create
    '''
    items_per_bulk = 999 / num
    if not item_list:
        return
    model = item_list[0].__class__
    groups = izip_longest(*[iter(item_list)] * items_per_bulk, fillvalue=None)
    for group in groups:
        group = [x for x in group if x]
        model.objects.bulk_create(group)

