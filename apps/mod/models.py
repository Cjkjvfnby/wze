from django.db import models
from apps.admin.utils import _get_file
import re
from itertools import izip_longest

FILE_PATH = 'messages/strings/names.txt'
reg = re.compile('(?P<key>[\w\d-]+)[\t ]*(_\()*\"(?P<name>.*)\"')
# Create your models here.
class Name(models.Model):
    key = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=64)


def load_names():
    Name.objects.all().delete()
    lines = _get_file(FILE_PATH)
    res = []
    for line in lines:
        match = re.match(reg, line[0])
        if match:
            res.append(Name(key=match.group('key'), name=match.group('name')))
    items_per_bulk = 999 / 3
    groups = izip_longest(*[iter(res)] * items_per_bulk, fillvalue=None)
    for group in groups:
        group = [x for x in group if x]
        Name.objects.bulk_create(group)



