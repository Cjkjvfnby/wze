from time import time
from django.db import transaction
from apps.research.admin import ResearchAdmin_Cam1, ResearchAdmin_Cam2, ResearchAdmin_Cam3, ResearchAdmin_Multiplayer

def load(*list_of_models):
    with transaction.commit_on_success():
        [model.load() for model in list_of_models]

def load_all():
    t1 = time()

    from apps.weapon.models import Weapon, ECM, Sensor, Repair, Construction, Brain, WeaponSound
    from apps.structure.models import Structure, StructureDefence, StructureWeapon, BodyDefence, Feature
    from apps.function.models import StructureFunction, Function
    from apps.body.models import Body, Propulsion, PropulsionSound, PropulsionType, BodyPropulsion
    from apps.mod.models  import load_names
    from apps.templates.models import Template, TemplateWeapon
    from apps.research.models import (Research_Cam1, ResearchFunctions_Cam1, ResearchPreRequisites_Cam1, ResultStructure_Cam1,
                                      ResearchStructure_Cam1, ResultComponent_Cam1,
                                      ResearchObsoleteComponent_Cam1, ResearchObsoletStructure_Cam1)
    from apps.research.models import (Research_Cam2, ResearchFunctions_Cam2, ResearchPreRequisites_Cam2, ResultStructure_Cam2,
                                      ResearchStructure_Cam2, ResultComponent_Cam2,
                                      ResearchObsoleteComponent_Cam2, ResearchObsoletStructure_Cam2)
    from apps.research.models import (Research_Cam3, ResearchFunctions_Cam3, ResearchPreRequisites_Cam3, ResultStructure_Cam3,
                                      ResearchStructure_Cam3, ResultComponent_Cam3,
                                      ResearchObsoleteComponent_Cam3, ResearchObsoletStructure_Cam3)
    from apps.research.models import (Research_Multiplayer, ResearchFunctions_Multiplayer, ResearchPreRequisites_Multiplayer, ResultStructure_Multiplayer,
                                      ResearchStructure_Multiplayer, ResultComponent_Multiplayer,
                                      ResearchObsoleteComponent_Multiplayer, ResearchObsoletStructure_Multiplayer)


    
    load_names()
    load(    
        Feature,
        Construction,
        ECM,
        Sensor,
        Repair,
        Weapon,
        StructureDefence,
        Body,
        PropulsionType,
        Function,
        BodyDefence,
        Research_Cam1,Research_Cam2,Research_Cam3,Research_Multiplayer)

    load(Structure)
#        Propulsion,
#        Brain,
#        WeaponSound,
#        ResearchFunctions_Cam1,ResearchFunctions_Cam2,ResearchFunctions_Cam3,ResearchFunctions_Multiplayer,
#        ResearchPreRequisites_Cam1,ResearchPreRequisites_Cam2,ResearchPreRequisites_Cam3,ResearchPreRequisites_Multiplayer)
#
#    load(ResearchStructure_Cam1,ResearchStructure_Cam2,ResearchStructure_Cam3,ResearchStructure_Multiplayer,
#        ResultComponent_Cam1,ResultComponent_Cam2,ResultComponent_Cam3,ResultComponent_Multiplayer,
#        ResultStructure_Cam1,ResultStructure_Cam2,ResultStructure_Cam3,ResultStructure_Multiplayer,
#        ResearchObsoleteComponent_Cam1,ResearchObsoleteComponent_Cam2,ResearchObsoleteComponent_Cam3,ResearchObsoleteComponent_Multiplayer,
#        ResearchObsoletStructure_Cam1,ResearchObsoletStructure_Cam2,ResearchObsoletStructure_Cam3,ResearchObsoletStructure_Multiplayer,
#        StructureWeapon,
#        PropulsionSound,
#        StructureFunction,
#        BodyPropulsion,
#        Template)
#
#    load(TemplateWeapon)

    for cls in (ResearchAdmin_Cam1, ResearchAdmin_Cam2, ResearchAdmin_Cam3, ResearchAdmin_Multiplayer):
        if cls.model.objects.count():
            cls.visible = True
        else:
            cls.visible = False


    from django.contrib.admin.models import LogEntry, ADDITION
    from django.contrib.auth.models import User

    from django.contrib.contenttypes.models import ContentType


    user = User.objects.latest('pk')
    LogEntry.objects.log_action(
        user_id         = user.id,
        content_type_id = ContentType.objects.get_for_model(user).pk,
        object_id       = user.pk,
        object_repr     = 'Stats was loaded',
        action_flag     = ADDITION,
        )


    return time() - t1