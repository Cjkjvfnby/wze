from ConfigParser import ConfigParser

class WZConfigParser(ConfigParser):
    '''Not lowercase names'''
    def optionxform(self, optionstr):
        return optionstr