from django.db import models
from django.db.models.base import ModelBase
from itertools import izip_longest
from apps.admin.fields import WZField
from utils import get_file

from apps.mod.models import Name
from itertools import  count
from wzconfigparser import WZConfigParser as ConfigParser
import os
from django.conf import settings

class WZTemplate(list):
    def __init__(self):
        self.names = []


    def append(self, p_object):
        self.names.append(p_object.name)
        return super(WZTemplate, self).append(p_object)

    def sort(self):
        super(WZTemplate, self).sort(key=lambda x: x.creation_counter)
        self.names = [x.name for x in self]


    def __str__(self):
        return ', '.join([str(x) for x in self])


    def get_headers(self):
        return ','.join([x.name for x in self])

    def get_line(self, obj):
        line = []
        for field in self:
            attr = getattr(obj, field.name)
            line.append(field.to_csv(obj, attr))
        return ','.join(line)

    def add_validation_to_form(self, form):
        [field.set_validator(form) for field in self if field.visible]


class WZModelBase(ModelBase):
    def __new__(cls, name, bases, attrs):

        is_abstract =   getattr(attrs.get('Meta', None), 'abstract', False)
        if '__metaclass__' not in attrs:
            template = WZTemplate()

            for key, val in attrs.items():
                if isinstance(val, WZField):
                    template.append(val)



            if not is_abstract:
                attrs['counter'] = models.PositiveIntegerField(default=99999)


            if hasattr(bases[0], '_template'):
                [template.append(x) for x in bases[0]._template]
            template.sort()

            attrs['_template'] = template
        new_class =  super(WZModelBase, cls).__new__(cls, name, bases, attrs)



        if hasattr(new_class, '_template'):
            new_class._template.sort()

        return new_class


class WZModel(models.Model):
    HEADER = ''
    FILE_PATH = ''
    load_from_first = False # used
    __metaclass__ = WZModelBase

    class Meta:
        abstract = True
        ordering = ['counter']

    def get_name(self):
        if 'key' in self._template.names:
            try:
                return Name.objects.get(key=self.key).name
            except Name.DoesNotExist:
                return self.key
        else:
            return '__unicode__ is not defined for %s' % self.__class__.__name__

    def __unicode__(self):
        return self.get_name()

    @classmethod
    def get_data(cls):
        if cls.FILE_PATH.endswith('.txt'):
            return cls.get_csv()
        elif cls.FILE_PATH.endswith('.ini'):
            return cls.get_config()
        else:
            raise Exception ('Unknown format')


    @classmethod
    def get_csv(cls):
        res = []
        if not cls.load_from_first:
            res.append(cls.HEADER)
        res.extend([cls._template.get_line(obj) for obj in cls.objects.all()])
        return '\n'.join(res)

    @classmethod
    def get_config(cls):
        '''
        class with no key will cause error
        '''
        text = []
        for obj in cls.objects.all().order_by('counter'):
            text.append('[%s]' % obj.key)
            for field in obj._template:
                if field.name != 'key':
                    if field.name == 'name':
                        text.append('name = "%s"' % obj.name)
                    else:
                        res =  getattr(obj, field.name)
                        if res == field.default:
                            pass
                        else:
                            text.append('%s = %s' % (field.name, res))
            text.append('')
        return '\n'.join(text)

    @classmethod
    def get_xml(cls):

        res = []
        for obj in cls.objects.all().order_by('counter'):
            res.append('<%s>' % cls.__name__)
            for field in obj._template:
                res.append('    <{0}>{1}</{0}>'.format(field.name, getattr(obj, field.name) ))
            res.append('</%s>' % cls.__name__)
        return '\n'.join(res)

    @classmethod
    def load(cls):
        if cls.FILE_PATH.endswith('.txt'):
            cls._load_csv()
        elif cls.FILE_PATH.endswith('.ini'):
            cls._load_config()
        else:
            raise Exception ('Unknown format')

    @classmethod
    def _load_config(cls):
        cls.objects.all().delete()
        counter = count(1)
        cp = ConfigParser()
        cp.read(cls.get_path())
        res = []
        for k,v in cp._sections.items():
            kwrgs = {'key':k, 'counter': counter.next()}
            print v.keys()
            for name in cls._template.names:
                if name in v:
                    kwrgs[name] = v[name].strip('"\'')
            res.append(cls(**kwrgs))
        cls.bulk_save(res)

    @classmethod
    def get_path(cls):
        return os.path.join(settings.PATH_TO_MOD, cls.FILE_PATH)


    @classmethod
    def _load_csv(cls):
        cls._counter = count(1)
        lines = get_file(cls)
        cls.objects.all().delete()

        if not cls.load_from_first:
            lines = lines[1:]
        res = [cls.from_csv(line) for line in lines]
        cls.bulk_save(res)
        del cls._counter


    @classmethod
    def from_csv(cls, line):
        fields = cls._template.names

        obj = cls()
        assert  len(fields) == len(line), 'Line and class field_list for  %s should be equal. %s != %s' % (
            repr(obj), len(fields), len(line))

        for key, val in zip(fields, line):
            setattr(obj, key, val)
        obj.counter = cls._counter.next()
        return obj

    @classmethod
    def bulk_save(cls, item_list):
        """
        sqlite limitation hack
        https://docs.djangoproject.com/en/dev/ref/models/querysets/#django.db.models.query.QuerySet.bulk_create
        """
        items_per_bulk = 999 / len(cls._meta.fields)
        if not item_list:
            return
        groups = izip_longest(*[iter(item_list)] * items_per_bulk, fillvalue=None)
        for group in groups:
            group = [x for x in group if x]
            cls.objects.bulk_create(group)

    @classmethod
    def get_template(cls):
        return cls._template


    @classmethod
    def get_class_name(cls):
        return cls.__name__













