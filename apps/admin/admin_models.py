from django.contrib import admin
from django.contrib.admin.options import InlineModelAdmin
from django.forms import MediaDefiningClass

class ModelMetaclass(MediaDefiningClass):
    def __new__(cls, name, bases, attrs):
        new_class = super(ModelMetaclass, cls).__new__(cls, name, bases, attrs)
        if new_class.model:
           if not hasattr(new_class.form, 'Meta'):
               new_class.exclude = ['counter'] + [x.name for x in new_class.model._template if not x.visible]

           if getattr(new_class,'list_display', []).__len__() == 1:
                new_class.list_display = list(new_class.list_display) + [x.name for x in new_class.model._template if x.visible ]
                if 'name' in new_class.list_display:
                    new_class.list_display.remove('name')

        return new_class

class WZInline(InlineModelAdmin):
    __metaclass__ = ModelMetaclass
    template = 'admin/edit_inline/tabular.html'


class WZModelAdmin(admin.ModelAdmin):
    model = None
    __metaclass__ = ModelMetaclass
    visible = True

    def get_form(self, request, obj=None, **kwargs):
        form =  super(WZModelAdmin, self).get_form(request, obj, **kwargs)
        if obj and hasattr(obj, 'key') and 'key' in form.base_fields:
            form.base_fields.pop('key')


        form.clean = lambda x: self.clean(x)#()elambda x : self.clean(x)
        return form

    def clean(self, inst):
        return inst.cleaned_data

    def get_model_perms(self, *args, **kwargs):
        if self.visible:
            return admin.ModelAdmin.get_model_perms(self, *args, **kwargs)
        return {}



def register(admin_class, hide=False, **options):
    if hide and not admin_class.model.objects.count():
        admin_class.visible = False
    model_or_iterable = admin_class.model
    admin.site.register(model_or_iterable, admin_class, **options)


# hide unused tables
from django.contrib.auth.models import Group, User

admin.site.unregister([Group, User])