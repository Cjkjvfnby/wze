from django.db import models
from django.db.models.fields.related import ReverseSingleRelatedObjectDescriptor
from django import forms



class DEFAULTS:
    UBYTE_MAX = 255
    SBYTE_MIN = -128
    SBYTE_MAX = 127
    MAX = [-2147483648, 2147483647] # hz
    UDWORD = [0, 4294967295]
    SDWORD = [-2147483648, 2147483647]
    UBYTE = [0, UBYTE_MAX]
    SBYTE = [SBYTE_MIN, SBYTE_MAX]


class ReverseSingleRelatedObjectKeyDescriptor(ReverseSingleRelatedObjectDescriptor):
    '''change it to load reladed model by string'''

    def __set__(self, instance, value):
        if value is not None and not isinstance(value, self.field.rel.to):
            try:
                value = self.field.rel.to.objects.get(key=value)
            except self.field.rel.to.DoesNotExist, e:
                pass
        super(ReverseSingleRelatedObjectKeyDescriptor, self).__set__(instance, value)


class NameForeignKey(models.ForeignKey):
    def contribute_to_class(self, cls, name):
        super(NameForeignKey, self).contribute_to_class(cls, name)
        setattr(cls, self.name, ReverseSingleRelatedObjectKeyDescriptor(self))


class WZField(models.Field):
    visible = True
    kwargs = {}
    help_text = "_"
    funct = lambda self, obj, x: x

    def set_validator(self, form):
        pass


    def __init__(self, *args, **kwargs):
        lim = kwargs.pop('limit', None)
        if not hasattr(self.__class__, 'limit'):
            self.limit = lim

        kwargs.update(self.kwargs)
        self.field.__init__(self, *args, **kwargs)

    def to_csv(self, obj, val):
        return self.funct(obj, val)


    def get_data(self):
        res = [self.verbose_name.capitalize(), self.get_doc(), self.get_limits(), self.get_default(), self.get_help()]
        if isinstance(self, WZUnusedField):
            res[0] = 'Unused'
        return res


    def get_default(self):
        return self.default

    def get_limits(self):
        text = []
        if self.choices:
            text.append('%s' % [x for x, y in self.choices])
        if self.unique:
            text.append('Unique')

        #        text.append('default=%s' % self.default)

        if self.max_length:
            text.append('max_length=%s' % self.max_length)
        if self.blank:
            text.append('Could be blank')
        return ', '.join(text)


    def get_doc(self):
        return self.__doc__

    def get_help(self):
        return self.help_text or self.__class__.help_text


class WZCharField(WZField, models.CharField):
    'text'
    field = models.CharField
    kwargs = {'max_length': 255}


class WZNameField(WZCharField):
    'text'
    kwargs = {'max_length': 255, 'primary_key': True}
    help_text = "Internal name of object"


class WZPieField(WZCharField):
    'text'
    def get_limits(self):
        return 'should be existing PIE file, ' + super(WZPieField, self).get_limits()


class WZIconField(WZCharField):
    'text'
    pass

    def get_limits(self):
        return 'should be ICON, ' + super(WZIconField, self).get_limits()




class WZPositiveIntegerField(WZField, models.PositiveIntegerField):
    'number'
    field = models.PositiveIntegerField
    funct = lambda self, obj, x: str(x)
    limit = DEFAULTS.MAX


    def set_validator(self, form):
        name = self.name
        def validate(instance):
            data = instance.cleaned_data[name]
            if not (self.limit[0] <= data <= self.limit[1]):
                raise forms.ValidationError("%s out of limits %s" % (data, self.limit) )
            return data
        setattr(form, 'clean_%s' % name, validate)


    def get_limits(self):

        lim = super(WZPositiveIntegerField, self).get_limits()
        if lim:
            lim += ', '
        return 'limit=%s' % self.limit


class WZIntegerField(WZField, models.IntegerField):
    'number'
    field = models.IntegerField
    funct = lambda self, obj, x: str(x)
    limit = DEFAULTS.MAX

    def set_validator(self, form):

        name = self.name
        def validate(instance):
            data = instance.cleaned_data[name]
            if not self.limit[0] <= data <= self.limit[1]:
                raise forms.ValidationError("%s out of limits %s" % (data, self.limit) )
            return data
        setattr(form, 'clean_%s' % name, validate)


    def get_limits(self):
        lim = super(WZIntegerField, self).get_limits()
        if lim:
            lim += ', '
        return 'limit=%s' % self.limit


CHOICES_INT = (
    ('1', 'Yes'),
    ('0', 'No'),
    )

CHOICES_CHAR = (
    ('YES', 'Yes'),
    ('NO', 'No'),
    )

CHOICES_BOOL = (
    ('true', 'Yes'),
    ('false', 'No'),
    )

class WZForeignKey(WZField, NameForeignKey):
    'text'
    field = NameForeignKey
    funct = lambda self, obj,  x: x.key

    def get_limits(self):
        href = '<a href="#{0}">{0}</a>'.format(self.rel.to.__name__)
        return 'One of the %s keys' % href


class WZBooleanIntField(WZField, models.CharField):
    'Choice'
    kwargs = {'choices': CHOICES_INT, 'max_length': 64}
    field = models.CharField

class WZBooleanBoolField(WZField, models.CharField):
    'Choice'
    kwargs = {'choices': CHOICES_BOOL, 'max_length': 64}
    field = models.CharField



class WZBooleanCharField(WZField, models.CharField):
    'Choice'
    kwargs = {'choices': CHOICES_CHAR, 'max_length': 64}
    field = models.CharField


class WZUnusedField(WZCharField):
    'text'
    visible = False
    help_text = 'unused'
    #    funct = lambda self, x : 'unused'

    def get_limit(self):
        return 'max_length=%s' % self.max_length



