import os
from zipfile import ZipFile
from itertools import izip_longest
from django.conf import settings

def get_diff(cls, new_text):
    diff = []
    text = [','.join(x) for x in get_file(cls)]
    new_text = new_text.split('\n')

    if not cls.load_from_first:
        text = text[1:]
        new_text = new_text[1:]
    text_set = set([x.strip('\n') for x in text])
    new_text_set = set(new_text)

    red_diff = text_set - new_text_set
    green_diff = new_text_set - text_set

    diff.extend([('red', x)   for x in (red_diff)])
    diff.extend([('green', x) for x in (green_diff)])
    return cls.FILE_PATH, diff


def set_header(cls):
    text = get_file(cls)[0]
    cls.HEADER = ','.join(text)



def save_to_file(cls, text, format=None):
    if format:
        file = '.'.join(cls.FILE_PATH.split('.')[:-1]) + '.%s' % format
    else:
        file = cls.FILE_PATH

    path = os.path.join(settings.PATH_TO_MOD, file)
    with open(path, 'wb') as f:
        f.write(text)
        f.write('\n')


def save_to_zip(cls, zipinfo, text, format=None):
    if format:
        file = '.'.join(cls.FILE_PATH.split('.')[:-1]) + format
    else:
        file = cls.FILE_PATH
    zipinfo.writestr(file, text)


def get_file(cls):
    file_name = cls.FILE_PATH
    return _get_file(file_name)


def _get_file(file_name):
    '''to use outside the class'''
    path = settings.PATH_TO_MOD
    if settings.MOD_SOURCE:
        lines = get_file_from_folder(path, file_name)
    else:
        lines = get_file_from_zip(path, file_name)
    lines = [line.strip('\n') for line in lines]
    return [x.split(',') for x in lines if x]


def get_file_from_folder(path, file_name):
    file_path = os.path.join(path, file_name)
    assert os.path.exists(file_path), 'Wrong path %s ' % file_path
    return open(file_path).readlines()


def get_file_from_zip(path, file_name):
    zf = ZipFile(path)
    assert file_name in zf.namelist(), 'Filename is not present in archive %s' % file_name
    return zf.readlines(file_name)


def bulk_save(item_list, num):
    '''
    sqlite limitation hack
    https://docs.djangoproject.com/en/dev/ref/models/querysets/#django.db.models.query.QuerySet.bulk_create
    '''
    items_per_bulk = 999 / num
    if not item_list:
        return
    model = item_list[0].__class__
    groups = izip_longest(*[iter(item_list)] * items_per_bulk, fillvalue=None)
    for group in groups:
        group = [x for x in group if x]
        model.objects.bulk_create(group)

