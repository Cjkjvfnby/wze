from django.contrib import admin
from models import Template, TemplateWeapon
from apps.admin.admin_models import register, WZModelAdmin, WZInline

class WeaponsInline(WZInline):
    model = TemplateWeapon
    extra = 1
    max_num = 3


class TemplateAdmin(WZModelAdmin):
    inlines = [WeaponsInline]
    model = Template


register(TemplateAdmin)
