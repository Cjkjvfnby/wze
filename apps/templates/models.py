from apps.admin.fields import (WZCharField, WZNameField, WZPositiveIntegerField,
                               WZForeignKey, WZBooleanIntField, WZUnusedField )
from apps.admin.model import WZModel
from apps.admin.utils import get_file
from apps.body.models import Body, Propulsion
from apps.weapon.models import ECM, Repair, Sensor, Brain, Construction, Weapon

TEMPLATE_TYPE = ((x, x) for x in ('DROID', 'CYBORG', 'TRANSPORTER', 'PERSON'))

# Create your models here.
class Template(WZModel):
    load_from_first = True
    FILE_PATH = 'stats/templates.txt'
    key = WZNameField()
    id = WZPositiveIntegerField(unique=True,
        help_text='Template ID, only used by specific scripting functions it seems. Needs to be unique?')
    body = WZForeignKey(Body)
    brain = WZForeignKey(Brain, default='ZNULLBRAIN')
    construct = WZForeignKey(Construction, default='ZNULLCONSTRUCT')
    ecm = WZForeignKey(ECM, default='ZNULLECM')
    availible = WZBooleanIntField(help_text='YES/NO ,defines whether the template can be built by human player.')
    propulsion = WZForeignKey(Propulsion)
    repair = WZForeignKey(Repair, default='ZNULLREPAIR')
    type = WZCharField(choices=TEMPLATE_TYPE)
    sensor = WZForeignKey(Sensor, default='DefaultSensor1Mk1')
    num_weaporns = WZPositiveIntegerField(
        help_text="The number of weapons of this template, needs to correspond assignweapons.txt's weapon assignments.")


class TemplateWeapon(WZModel):
    FILE_PATH = 'stats/assignweapons.txt'
    load_from_first = True

    templ = WZForeignKey(Template, verbose_name='Template')
    weapon = WZForeignKey(Weapon, related_name='templateweaporn', blank=True, null=True)
    number = WZUnusedField()


    @classmethod
    def get_csv(cls):
        res = []
        for template in Template.objects.exclude(templateweapon=None):
            line = [template.key]
            template_weapon_set = template.templateweapon_set.all()
            line.extend([wp.weapon.key for wp in template_weapon_set])
            line.extend(['NULL'] * (4 - len(line)))
            line.append(str(template_weapon_set[0].number))
            res.append(','.join(line))
        return '\n'.join(res)


    def __unicode__(self):
        return '%s of %s' % (self.weapon.get_name(), self.templ.get_name())

    @classmethod
    def load(cls):
        lines = get_file(cls)
        cls.objects.all().delete()
        res = []
        for line in lines:
            template_key, wp1, wp2, wp3, number = line

            template = Template.objects.get(key=template_key)

            res.extend([TemplateWeapon(
                templ=template,
                weapon=Weapon.objects.get(key=key),
                number=number) for key in (wp1, wp2, wp3) if key != 'NULL'])
        cls.bulk_save(res)
