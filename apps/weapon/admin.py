from models import Weapon, ECM, Sensor, Repair, Construction, WeaponSound
from django.contrib import admin
from apps.admin.admin_models import register, WZInline, WZModelAdmin

#class WeaponSoundAdmin(WZModelAdmin):
#    model = WeaponSound
#
#register(WeaponSoundAdmin)


class WeaponSoundInline(WZInline):
    model = WeaponSound
    extra = 0
    max_num = 1

    def has_delete_permission(self, request, obj=None):
        return False





class WeapornAdmin(WZModelAdmin):
    model = Weapon
    list_display = ('__str__', 'key', 'weapon_class', 'weapon_sub_class', 'penetrate', 'designable')
    inlines = [WeaponSoundInline]

    search_fields = ('key',)
    fieldsets = (
        ('Stats', {
            'fields': (('build_power', 'build_points'), 'body', 'weight',)
        }),
        (None, {
            'fields': ('designable',)
        }),

        ('Shoots', {
            'fields': (
                ('short_range', 'long_range',),
                ('short_hit', 'long_hit',),
                'fire_pause',

                'num_rounds',
                'reload_time',
                'damage',
                ('radius', 'radius_hit', 'radius_damage',),
                ('incen_time', 'incen_damage', 'incen_radius'),
                ('direct_life', 'radius_life'),
                'flight_speed',
                'indirect_height',
                'fire_on_move',
                ('weapon_class', 'weapon_sub_class',),
                'movement',
                'weapon_effect',
                'rotate',
                ('max_elevation', 'min_elevation',),
                ('face_player', 'face_in_flight',),
                'recoil_value',
                'min_range',
                'light_world',
                'effect_size',
                'surface_to_air',
                'num_attack_runs',
                'penetrate'
                )
        }),
        ('PIES', {
            'classes': ('collapse',),
            'fields': ('gfx_file',
                       'mount_gfx',
                       'muzzle_gfx',
                       'flight_gfx',
                       'hit_gfx',
                       'miss_gfx',
                       'water_gfx',
                       'trail_gfx',)
        }),
        )

    list_filter = ('designable', 'weapon_class', 'weapon_sub_class',)

register(WeapornAdmin)


class ECMAdmin(WZModelAdmin):
    model = ECM


class SensorAdmin(WZModelAdmin):
    model = Sensor


class RepairAdmin(WZModelAdmin):
    model = Repair


class ConstructionAdmin(WZModelAdmin):
    model = Construction

register(ECMAdmin)
register(SensorAdmin)
register(RepairAdmin)
register(ConstructionAdmin)

