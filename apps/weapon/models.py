from apps.admin.fields import (WZCharField, WZNameField, WZPieField, WZPositiveIntegerField, WZForeignKey,
                               WZBooleanCharField, WZUnusedField, WZBooleanIntField, WZIntegerField, DEFAULTS)
from apps.admin.model import WZModel


def choice_generator(*args):
    return [(x, x) for x in args]


FIRE_ON_MOVE = choice_generator('YES', 'NO', 'PARTIAL')
WEAPORN_CLASS = choice_generator('HEAT', 'KINETIC')
PROJECTIVE_TRACE = choice_generator('DIRECT', 'INDIRECT', 'HOMING-DIRECT', 'ERRATIC-DIRECT', 'SWEEP')
SURFACE_TO_AIR = (('0', 'Ground'), ('1', 'Air'), ('100', 'Both'),)
DAMAGE_TYPE = choice_generator('ANTI PERSONNEL', 'ANTI TANK',
    'BUNKER BUSTER', 'ARTILLERY ROUND', 'FLAMER', 'ANTI AIRCRAFT', 'ALL ROUNDER')

SUB_CLASS = choice_generator('MACHINE GUN', 'COMMAND', 'ROCKET', 'CANNON', 'ENERGY',
    'A-A GUN', 'GAUSS', 'MISSILE', 'BOMB', 'FLAME', 'LAS_SAT',
    'MORTARS', 'HOWITZERS', 'ELECTRONIC', 'SLOW ROCKET')

class Weapon(WZModel):
    FILE_PATH = 'stats/weapons.txt'
    key = WZNameField()
    tech_level = WZUnusedField()
    build_power = WZPositiveIntegerField(help_text='The price in energy.')
    build_points = WZPositiveIntegerField(
        help_text='Buildtime = buildpoints/buildpoints per second.')
    weight = WZPositiveIntegerField(help_text='Weight; may affect droid speed.', limit=DEFAULTS.UDWORD)
    hp = WZUnusedField() # todo &weaponsize  should be checked
    system_points = WZUnusedField()
    body = WZPositiveIntegerField(
        help_text='HP of the weapon. total HP = 1 + body HP x (100+propulsion HP)/100 + turret(s) HP')
    gfx_file = WZPieField(help_text='Pie model of weapon body.')
    mount_gfx = WZPieField(help_text='Pie model of weapon barrel.')
    muzzle_gfx = WZPieField(help_text='Muzzle flash pie model displayed upon firing.')
    flight_gfx = WZPieField(help_text='Projectile pie model used by weapon.')
    hit_gfx = WZPieField(help_text='Impact pie model when it hits objects.')
    miss_gfx = WZPieField(help_text='Miss pie model when it misses and hits ground.')
    water_gfx = WZPieField(help_text='Water pie model when it misses and hits water.')
    trail_gfx = WZPieField(help_text='Weapon trail pie model during flight.', default='NULL')
    short_range = WZPositiveIntegerField(help_text='Range of weapon to use short-range accuracy (1/128 tiles).', limit=DEFAULTS.UDWORD)
    long_range = WZPositiveIntegerField(help_text='Range of weapon to use long-range accuracy (1/128 tiles).', limit=DEFAULTS.UDWORD)
    short_hit = WZPositiveIntegerField(help_text='Hit chance of shortRange shots.', limit=DEFAULTS.UDWORD)
    long_hit = WZPositiveIntegerField(help_text='Hit chance of longRange shots.', limit=DEFAULTS.UDWORD)
    fire_pause = WZPositiveIntegerField(help_text='Pause between shots (1/10 seconds).', limit=DEFAULTS.UDWORD)
    num_explosions = WZUnusedField()
    num_rounds = WZPositiveIntegerField(
        help_text='The number of rounds of a salvo weapon. (Ripple angel etc). 0 = not a salvo weapon', limit=DEFAULTS.UDWORD)
    reload_time = WZPositiveIntegerField(
        help_text='Time required to fully reload a salvo weapon. For non-salvo weapons see firePause.', limit=DEFAULTS.UDWORD)
    damage = WZPositiveIntegerField(help_text='Weapon damage.', limit=DEFAULTS.UDWORD)
    radius = WZPositiveIntegerField(help_text='The splash radius of weapon.')
    radius_hit = WZPositiveIntegerField(help_text='Chance to do splash damage.', limit=DEFAULTS.UDWORD)
    radius_damage = WZPositiveIntegerField(help_text='Splash damage.')
    incen_time = WZPositiveIntegerField(help_text='Time the target stays on fire (1/10 seconds).')
    incen_damage = WZPositiveIntegerField(help_text='Incendiary damage per second target takes while being on fire.', limit=DEFAULTS.UDWORD)
    incen_radius = WZPositiveIntegerField(help_text='Splash radius of on-fire effect.')
    direct_life = WZPositiveIntegerField(help_text='Time to live after impact (flame weapon projectile)')
    radius_life = WZPositiveIntegerField(help_text="Splash's time to live after impact (flame weapon)")
    flight_speed = WZPositiveIntegerField(help_text='Velocity of weapon project (1/128 tiles per 1/10 second).', limit=[1,DEFAULTS.MAX], default=500)
    indirect_height = WZPositiveIntegerField(help_text='Indirect weapon flight height.')
    fire_on_move = WZCharField(choices=FIRE_ON_MOVE)
    weapon_class = WZCharField(choices=WEAPORN_CLASS, default='KINETIC')
    weapon_sub_class = WZCharField(choices=SUB_CLASS,
        help_text='Weapon subclass. Controls which upgrades the weapon is affected by.')
    movement = WZCharField(choices=PROJECTIVE_TRACE)
    weapon_effect = WZCharField(choices=DAMAGE_TYPE,
        help_text="Weapon type. Controls what targets it's effective against (modifiers).")
    rotate = WZPositiveIntegerField(help_text='Turret rotation limits of weapon', limit=DEFAULTS.UBYTE)
    max_elevation = WZIntegerField(help_text='Max pitch of turret. Range: [090]', limit=DEFAULTS.UBYTE)
    min_elevation = WZIntegerField(help_text='Min pitch of turret. Range: [0-90]', limit=DEFAULTS.SBYTE)
    face_player = WZBooleanCharField(
        help_text='YES = HitGfx MissGfx and WaterGfx always face player view; NO = impact Gfx do not face player view')
    face_in_flight = WZBooleanCharField(
        help_text='YES = projectile always face player view; NO = projectile always not face player view')
    recoil_value = WZPositiveIntegerField(help_text='Recoil value of turret.')
    min_range = WZPositiveIntegerField(help_text='Minimal range of indirect weapon.')
    light_world = WZBooleanCharField(help_text='Lights up terrain around where it hits')
    effect_size = WZPositiveIntegerField(help_text='Size(scale) of special/particle effects of weapon.', limit=DEFAULTS.UBYTE)
    surface_to_air = WZCharField(choices=SURFACE_TO_AIR, help_text='0 = ground only; 1 = air only; 100 = both')
    num_attack_runs = WZPositiveIntegerField(help_text='Number of attack runs of vtol.', limit=DEFAULTS.UBYTE)
    designable = WZBooleanIntField(help_text='Whether weapon appears in design screen.')
    penetrate = WZBooleanIntField(
        help_text='Projectile goes through target and can continue and damage objects behind it.')


LOCATION = [(x, x) for x in ('TURRET', 'DEFAULT')]
SENSOR_TYPE = ((x, x) for x in ('STANDARD', 'INDIRECT', 'CB', 'VTOL INTERCEPT', 'SUPER'))

class Sensor(WZModel):
    FILE_PATH = 'stats/sensor.txt'
    key = WZNameField()
    tech_level = WZUnusedField()
    build_power = WZPositiveIntegerField(help_text='The price in energy.')
    build_points = WZPositiveIntegerField(
        help_text='Buildtime = buildpoints/buildpoints per second.')
    weight = WZPositiveIntegerField(help_text='Weight; may affect droid speed.', limit=DEFAULTS.UDWORD)
    Ignored1 = WZUnusedField()
    Ignored2 = WZUnusedField()
    hp = WZPositiveIntegerField(help_text='HP of sensor')
    model = WZPieField()
    mount_model = WZPieField()
    range = WZPositiveIntegerField(help_text='View range (1/128 tiles))')
    location = WZCharField(choices=LOCATION)
    type = WZCharField(choices=SENSOR_TYPE)
    time = WZUnusedField()
    power = WZPositiveIntegerField()
    disignabel = WZBooleanIntField(help_text="1 = Body appears in design screen. 0 = It doesn't.")


class ECM(WZModel):
    FILE_PATH = 'stats/ecm.txt'
    load_from_first = True
    key = WZNameField()
    unused1 = WZUnusedField()
    build_power = WZPositiveIntegerField(help_text='The price in energy.')
    build_points = WZPositiveIntegerField(
        help_text='Buildtime = buildpoints/buildpoints per second.')
    weight = WZPositiveIntegerField(help_text='Weight; may affect droid speed.', limit=DEFAULTS.UDWORD)
    unused5 = WZUnusedField()
    unused6 = WZUnusedField()
    hp = WZPositiveIntegerField(help_text='HP of the body.')
    model = WZPieField()
    mount_model = WZPieField()
    location = WZCharField(choices=LOCATION)
    power = WZPositiveIntegerField()
    range = WZPositiveIntegerField(limit=DEFAULTS.UDWORD)
    disignable = WZBooleanIntField(help_text="1 = Body appears in design screen. 0 = It doesn't.")


class Repair(WZModel):
    FILE_PATH = 'stats/repair.txt'
    load_from_first = True
    key = WZNameField()
    unused1 = WZUnusedField()
    build_power = WZPositiveIntegerField(help_text='The price in energy.')
    build_points = WZPositiveIntegerField(
        help_text='Buildtime = buildpoints/buildpoints per second.')
    weight = WZPositiveIntegerField(help_text='Weight; may affect droid speed.', limit=DEFAULTS.UDWORD)
    unused2 = WZUnusedField()
    unused3 = WZUnusedField()
    repairArmour = WZPositiveIntegerField()
    location = WZCharField(choices=LOCATION)
    model = WZPieField()
    mount_model = WZPieField()
    repair_points = WZPositiveIntegerField(limit=DEFAULTS.UDWORD)
    time = WZPositiveIntegerField()
    disignabel = WZBooleanIntField(help_text="1 = Body appears in design screen. 0 = It doesn't.")


class Construction(WZModel):
    FILE_PATH = 'stats/construction.txt'
    load_from_first = True

    key = WZNameField()
    unused1 = WZUnusedField()
    build_power = WZPositiveIntegerField(help_text='The price in energy.')
    build_points = WZPositiveIntegerField(
        help_text='Buildtime = buildpoints/buildpoints per second.')
    weight = WZPositiveIntegerField(help_text='Weight; may affect droid speed.', limit=DEFAULTS.UDWORD)
    unused2 = WZUnusedField()
    unused3 = WZUnusedField()
    hp = WZPositiveIntegerField(help_text='HP of the body.')
    model = WZPieField(help_text='Counstruction model')
    mount_model = WZPieField(help_text='Counstruction mount model')
    construct_points = WZPositiveIntegerField(limit=DEFAULTS.UDWORD)
    disignabel = WZBooleanIntField(help_text="1 = Body appears in design screen. 0 = It doesn't.")


class Brain(WZModel):
    FILE_PATH = 'stats/brain.txt'
    load_from_first = True
    key = WZNameField()
    unused1 = WZUnusedField()
    build_power = WZPositiveIntegerField(help_text='The price in energy.')
    build_points = WZPositiveIntegerField(
        help_text='Buildtime = buildpoints/buildpoints per second.')
    weight = WZPositiveIntegerField(help_text='Weight; may affect droid speed.', limit=DEFAULTS.UDWORD)
    unused2 = WZUnusedField()
    unused3 = WZUnusedField()
    weapon = WZForeignKey(Weapon)
    unused4 = WZUnusedField()


class WeaponSound(WZModel):
    FILE_PATH = 'stats/weaponsounds.txt'
    load_from_first = True
    weapon = WZForeignKey(Weapon)
    fire_sound = WZCharField()
    explosion_sound = WZCharField()
    unused = WZUnusedField()

    def __unicode__(self):
        return 'Sounds for %s' % self.weapon

