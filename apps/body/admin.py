from models import Body, Propulsion, PropulsionSound, PropulsionType
from django.contrib import admin
from apps.admin.admin_models import  register, WZModelAdmin, WZInline

class PropulsionSoundInline(WZInline):
    model = PropulsionSound
    extra = 0


class BodyAdmin(WZModelAdmin):
    model = Body


register(BodyAdmin)


class PropulsionAdmin(WZModelAdmin):
    model = Propulsion

register(PropulsionAdmin)


class PropulsionTypeAdmin(WZModelAdmin):
    model = PropulsionType
    inlines = [PropulsionSoundInline]

register(PropulsionTypeAdmin)


#class PropulsionSoundAdmin(WZModelAdmin):
#    model = PropulsionSound
#
#register(PropulsionSoundAdmin)
