from models import Body

def body_disignable_inspector():
    name = 'Not disignable bodies'
    items = []
    items = ['%s "%s"' % (body.key, body.name) for body in Body.objects.filter(designable='false')]
    if items:
        return (name, items)
    else:
        return None


