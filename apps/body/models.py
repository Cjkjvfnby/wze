from apps.admin.fields import (WZCharField, WZNameField, WZPieField, WZPositiveIntegerField,
                               WZForeignKey, WZBooleanIntField, WZUnusedField, WZBooleanBoolField)
from apps.admin.model import WZModel

SIZE = [(x,x) for x in ('SUPER HEAVY', 'LIGHT', 'MEDIUM','HEAVY','HEAVY',)]

DROID_TYPE = [(x,x) for x in ['PERSON', 'TRANSPORTER', 'DROID',
                              'CYBORG', 'CYBORG_SUPER',
                              'CYBORG_CONSTRUCT', 'CYBORG_REPAIR', ]]


# Create your models here.
class Body(WZModel):
    FILE_PATH = 'stats/body.ini'
    key = WZNameField()
    name = WZCharField()
    size = WZCharField(choices=SIZE, help_text='Armor type?')
    buildPower = WZPositiveIntegerField(help_text='The price in energy.')
    buildPoints = WZPositiveIntegerField(
        help_text='The buildpoints of a body, affects time to build. Buildtime = buildpoints/factory buildpoints per second ')
    weight = WZPositiveIntegerField(help_text='Weight of body; may affect droid speed.')
    hitpoints = WZPositiveIntegerField(help_text='HP of the body.')
    model = WZPieField(help_text='Pie model of body.')
    weaponSlots = WZPositiveIntegerField(
        help_text='Maximum number of weapons that can be installed on this body, can currently be up to 3.')
    powerOutput = WZPositiveIntegerField(help_text='Power output of the engine, may affect droid speed.')

    armourKinetic = WZPositiveIntegerField(
        help_text='the kinetic armor anything but flame/laser/incendiary/firebomb/lassat is kinetic weapon and damages kinetic damage.')
    armourHeat = WZPositiveIntegerField(help_text='the thermal armor')
    flameModel = WZPieField(help_text='other Pie model.', blank=True, default='')
    designable = WZBooleanBoolField(help_text="Body appears in design screen. or doesn't.")
    droidType = WZCharField(choices=DROID_TYPE, default='DROID')

    class Meta:
        verbose_name_plural = 'Bodies'

    def __unicode__(self):
        return self.name

TERRAIN = (
    ('GROUND', 'GROUND'),
    ("AIR", "AIR")
    )

class PropulsionType(WZModel):
    FILE_PATH = 'stats/propulsiontype.txt'
    load_from_first = True

    key = WZNameField()
    terrain = WZCharField(choices=TERRAIN)
    number = WZPositiveIntegerField()

    def __unicode__(self):
        return self.key


class PropulsionSound(WZModel):
    FILE_PATH = 'stats/propulsionsounds.txt'
    load_from_first = True

    key = WZForeignKey(PropulsionType)
    sound1 = WZCharField(max_length=64, blank=True)
    sound2 = WZCharField(max_length=64, blank=True)
    sound3 = WZCharField(max_length=64, blank=True)
    move_sound = WZCharField(max_length=64, blank=True)
    sound5 = WZCharField(max_length=64, blank=True)
    stop_sound = WZCharField(max_length=64, blank=True)
    sound7 = WZCharField(max_length=64, blank=True)

    def __unicode__(self):
        return 'Sound for %s' % self.key


class Propulsion(WZModel):
    FILE_PATH = 'stats/propulsion.txt'
    load_from_first = True

    key = WZNameField()
    tech_level = WZUnusedField(help_text='unused')
    build_power = WZPositiveIntegerField(help_text='The price in energy.')
    build_points = WZPositiveIntegerField(
        help_text='The buildpoints of a body, affects time to build. Buildtime = buildpoints/factory buildpoints per second ')
    weight = WZPositiveIntegerField(help_text='Weight of body; may affect droid speed.')
    hp = WZPositiveIntegerField(help_text='HP of the body.')
    ignored1 = WZUnusedField(help_text='unused')
    ignored2 = WZUnusedField(help_text='unused')
    IMD_name = WZCharField(max_length=64, help_text='IMD in design/research list')
    propulsion_type = WZForeignKey(PropulsionType)
    max_speed = WZCharField(max_length=64, help_text='unused')
    designable = WZBooleanIntField()


class BodyPropulsion(WZModel):
    FILE_PATH = 'stats/bodypropulsionimd.txt'
    load_from_first = True
    body = WZForeignKey(Body)
    propulsion = WZForeignKey(Propulsion)
    model_left = WZPieField()
    model_right = WZPieField()
    number = WZUnusedField()


