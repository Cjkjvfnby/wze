from django.forms import ModelForm
from models import ResearchObsoleteComponent_Cam1, Research_Cam1
from django.forms import CharField





class ResearchObsoleteComponent_Cam1Form(ModelForm):
    class Meta:
        model = ResearchObsoleteComponent_Cam1
        fields = ['component', 'type']

#    class Media:
#        js = ('/media/js/filtered-select.js', )






#class ResearchObsoleteComponent_Cam1(ResearchObsoleteComponent):
#    FILE_PATH = ResearchObsoleteComponent.FILE_PATH % ('cam1')
#    research = WZForeignKey(Research_Cam1)
#    component = WZCharField(help_text='Name for component')
#    type = WZCharField(choices=COMPONENT_TYPE)
#    unused = WZUnusedField()