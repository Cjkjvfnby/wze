from apps.admin.admin_models import register, WZInline, WZModelAdmin
from apps.research.models import (Research_Cam1, ResearchFunctions_Cam1, ResearchPreRequisites_Cam1, ResultStructure_Cam1,
                                  ResearchStructure_Cam1, ResultComponent_Cam1,
                                  ResearchObsoleteComponent_Cam1, ResearchObsoletStructure_Cam1, Research_Cam2, ResearchFunctions_Cam2, ResearchPreRequisites_Cam2, ResultStructure_Cam2,
                                  ResearchStructure_Cam2, ResultComponent_Cam2,
                                  ResearchObsoleteComponent_Cam2, ResearchObsoletStructure_Cam2,Research_Cam3, ResearchFunctions_Cam3, ResearchPreRequisites_Cam3, ResultStructure_Cam3,
                                  ResearchStructure_Cam3, ResultComponent_Cam3,
                                  ResearchObsoleteComponent_Cam3, ResearchObsoletStructure_Cam3,Research_Multiplayer, ResearchFunctions_Multiplayer, ResearchPreRequisites_Multiplayer, ResultStructure_Multiplayer,
                                  ResearchStructure_Multiplayer, ResultComponent_Multiplayer,
                                  ResearchObsoleteComponent_Multiplayer, ResearchObsoletStructure_Multiplayer)
from forms import ResearchObsoleteComponent_Cam1Form

def inline_factory(model, form=None):
    attrs = dict(extra=1, model=model, fk_name='research')
    if form:
        attrs['form'] = form

    cls = type.__new__(type, '%s_inline' % model.__name__, (WZInline,),
            attrs)
    return cls




class ResearchAdmin_Cam1(WZModelAdmin):
    search_fields = ('key',)
    inlines = [inline_factory(ResearchPreRequisites_Cam1),
               inline_factory(ResearchFunctions_Cam1),
               inline_factory(ResultStructure_Cam1),
               inline_factory(ResultComponent_Cam1),
               inline_factory(ResearchStructure_Cam1),
               inline_factory(ResearchObsoleteComponent_Cam1, form=ResearchObsoleteComponent_Cam1Form),
               inline_factory(ResearchObsoletStructure_Cam1),
               ]
    model = Research_Cam1

register(ResearchAdmin_Cam1, hide=True)

class ResearchAdmin_Cam2(WZModelAdmin):
    search_fields = ('key',)
    inlines = [inline_factory(ResearchPreRequisites_Cam2),
               inline_factory(ResearchFunctions_Cam2),
               inline_factory(ResultStructure_Cam2),
               inline_factory(ResultComponent_Cam2),
               inline_factory(ResearchStructure_Cam2),
               inline_factory(ResearchObsoleteComponent_Cam2),
               inline_factory(ResearchObsoletStructure_Cam2),
               ]
    model = Research_Cam2

register(ResearchAdmin_Cam2, hide=True)

class ResearchAdmin_Cam3(WZModelAdmin):
    search_fields = ('key',)
    inlines = [inline_factory(ResearchPreRequisites_Cam3),
               inline_factory(ResearchFunctions_Cam3),
               inline_factory(ResultStructure_Cam3),
               inline_factory(ResultComponent_Cam3),
               inline_factory(ResearchStructure_Cam3),
               inline_factory(ResearchObsoleteComponent_Cam3),
               inline_factory(ResearchObsoletStructure_Cam3),
               ]
    model = Research_Cam3

register(ResearchAdmin_Cam3, hide=True)

class ResearchAdmin_Multiplayer(WZModelAdmin):
    search_fields = ('key',)
    inlines = [inline_factory(ResearchPreRequisites_Multiplayer),
               inline_factory(ResearchFunctions_Multiplayer),
               inline_factory(ResultStructure_Multiplayer),
               inline_factory(ResultComponent_Multiplayer),
               inline_factory(ResearchStructure_Multiplayer),
               inline_factory(ResearchObsoleteComponent_Multiplayer),
               inline_factory(ResearchObsoletStructure_Multiplayer),
               ]
    model = Research_Multiplayer

register(ResearchAdmin_Multiplayer, hide=True)


