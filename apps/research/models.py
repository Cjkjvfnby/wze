from apps.admin.fields import (WZCharField, WZNameField, WZPieField, WZPositiveIntegerField,
                               WZForeignKey, WZUnusedField, WZIconField)
from apps.admin.model import WZModel
from apps.structure.models import Structure
from apps.function.models import Function
import os
from django.conf import settings
from django.contrib import admin


class WZResearchModel(WZModel):
    class Meta:
        abstract = True

    @classmethod
    def load(cls):
        if os.path.exists(os.path.join(settings.PATH_TO_MOD, cls.FILE_PATH)):
            super(WZResearchModel, cls).load()
        else:
            cls.objects.all().delete()
            if cls in admin.site._registry:
                admin.site.unregister(cls)


TECH_CHOICE = ((0, 'Major'),(1, 'Minor'))


COMPONENT_TYPE = [(x,x) for x in ('WEAPON', 'BODY', 'REPAIR', 'PROPULSION', 'SENSOR', 'CONSTRUCT', 'ECM')] + [('0', 'None')]
def get_component_link(key, comp_type):
    return '/admin/{0}/{0}/{1}'.format(comp_type.lower(), key)


class Research(WZResearchModel):
    '''determines a wide variety of stats for individual research topics, such as price, time-to-research, icon to use for representation, etc. For more information about this, go here.'''
    FILE_PATH = 'stats/research/%s/research.txt'
    load_from_first = True

    key = WZNameField()
    unused1 = WZUnusedField()
    subgroup_icon = WZIconField()
    major_tech = WZPositiveIntegerField(choices=TECH_CHOICE, help_text='Affects whether or not "Major research completed" is played, and whether or not it appears in Intelligence.')
    main_icon = WZIconField()
    imd = WZPieField(help_text='IMD to display for tech')
    imd2 = WZUnusedField()
    msg_name = WZCharField(help_text='Message in Intelligence screen for tech; must be zero for minor techs') # load messages
    structure_name = WZCharField(blank=True, help_text='if non-zero, use this for research graphics') # 0 or foreing key
    comp_name = WZCharField(help_text='if non-zero, use this for research graphics')
    comp_type = WZCharField(help_text='if previous was non-zero, indicate what type of component its')
    research_points = WZPositiveIntegerField(help_text='Points cost to research it (price = points/32, capped at 450)')
    key_topic = WZPositiveIntegerField(help_text='Is it a key topic? Visual effect only.')
    unused2 = WZUnusedField()
    unused3 = WZUnusedField()
    unused4 = WZUnusedField()
    unused5 = WZUnusedField()
    unused6 = WZUnusedField()
    unused7 = WZUnusedField()
    unused8 = WZUnusedField()
    class Meta:
        abstract = True

class Research_Cam1(Research):
    FILE_PATH = Research.FILE_PATH % ('cam1')
    class Meta:
        verbose_name = 'Research (camapain 1)'
        verbose_name_plural = 'Researchs (camapain 1)'
class Research_Cam2(Research):
    FILE_PATH = Research.FILE_PATH % ('cam2')
    class Meta:
        verbose_name = 'Research (camapain 2)'
        verbose_name_plural = 'Researchs (camapain 2)'
class Research_Cam3(Research):
    FILE_PATH = Research.FILE_PATH % ('cam3')
    class Meta:
        verbose_name = 'Research (camapain 3)'
        verbose_name_plural = 'Researchs (camapain 3)'

class Research_Multiplayer(Research):
    FILE_PATH = Research.FILE_PATH % ('multiplayer')
    load_from_first = False
    class Meta:
        verbose_name = 'Research (multiplayer)'
        verbose_name_plural = 'Researches (multiplayer)'



    
class ResearchPreRequisites(WZResearchModel):
    '''determines the prerequisites of research objects.'''
    FILE_PATH = 'stats/research/%s/prresearch.txt'
    load_from_first = True
    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s requaire %s' % (self.research, self.pre_requisites)

class ResearchPreRequisites_Cam1(ResearchPreRequisites):
    FILE_PATH = ResearchPreRequisites.FILE_PATH % ('cam1')
    research = WZForeignKey(Research_Cam1)
    pre_requisites = WZForeignKey(Research_Cam1, related_name='pre_requisites')
    unused1 = WZUnusedField()


class ResearchPreRequisites_Cam2(ResearchPreRequisites):
    FILE_PATH = ResearchPreRequisites.FILE_PATH % ('cam2')
    research = WZForeignKey(Research_Cam2)
    pre_requisites = WZForeignKey(Research_Cam2, related_name='pre_requisites')
    unused1 = WZUnusedField()


class ResearchPreRequisites_Cam3(ResearchPreRequisites):
    FILE_PATH = ResearchPreRequisites.FILE_PATH % ('cam3')
    research = WZForeignKey(Research_Cam3)
    pre_requisites = WZForeignKey(Research_Cam3, related_name='pre_requisites')
    unused1 = WZUnusedField()


class ResearchPreRequisites_Multiplayer(ResearchPreRequisites):
    FILE_PATH = ResearchPreRequisites.FILE_PATH % ('multiplayer')
    research = WZForeignKey(Research_Multiplayer)
    pre_requisites = WZForeignKey(Research_Multiplayer, related_name='pre_requisites')
    unused1 = WZUnusedField()
    
    
class ResultStructure(WZResearchModel):
    ''' determines which structures are made available upon researching the selected item.'''
    FILE_PATH = 'stats/research/%s/resultstructure.txt'
    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s for %s' % (self.structure, self.research)

class ResultStructure_Cam1(ResultStructure):
    FILE_PATH = ResultStructure.FILE_PATH % ('cam1')
    research = WZForeignKey(Research_Cam1)
    structure = WZForeignKey(Structure)
    unused1 = WZUnusedField()
    unused2 = WZUnusedField()


class ResultStructure_Cam2(ResultStructure):
    FILE_PATH = ResultStructure.FILE_PATH % ('cam2')
    research = WZForeignKey(Research_Cam2)
    structure = WZForeignKey(Structure)
    unused1 = WZUnusedField()
    unused2 = WZUnusedField()


class ResultStructure_Cam3(ResultStructure):
    FILE_PATH = ResultStructure.FILE_PATH % ('cam3')
    research = WZForeignKey(Research_Cam3)
    structure = WZForeignKey(Structure)
    unused1 = WZUnusedField()
    unused2 = WZUnusedField()


class ResultStructure_Multiplayer(ResultStructure):
    FILE_PATH = ResultStructure.FILE_PATH % ('multiplayer')
    research = WZForeignKey(Research_Multiplayer)
    structure = WZForeignKey(Structure)
    unused1 = WZUnusedField()
    unused2 = WZUnusedField()



class ResearchFunctions(WZResearchModel):
    '''determines which upgrades to apply upon researching the selected item.'''
    load_from_first = True
    FILE_PATH = 'stats/research/%s/researchfunctions.txt'

    class Meta:
        abstract = True

    def get_funct_link(self):
        return get_component_link(self.function.key, 'function')

class ResearchFunctions_Cam1(ResearchFunctions):
    FILE_PATH = ResearchFunctions.FILE_PATH % ('cam1')
    research = WZForeignKey(Research_Cam1)
    function = WZForeignKey(Function)
    unused = WZUnusedField()


class ResearchFunctions_Cam2(ResearchFunctions):
    FILE_PATH = ResearchFunctions.FILE_PATH % ('cam2')
    research = WZForeignKey(Research_Cam2)
    function = WZForeignKey(Function)
    unused = WZUnusedField()


class ResearchFunctions_Cam3(ResearchFunctions):
    FILE_PATH = ResearchFunctions.FILE_PATH % 'cam3'
    research = WZForeignKey(Research_Cam3)
    function = WZForeignKey(Function)
    unused = WZUnusedField()


class ResearchFunctions_Multiplayer(ResearchFunctions):
    FILE_PATH = ResearchFunctions.FILE_PATH % ('multiplayer')
    research = WZForeignKey(Research_Multiplayer)
    function = WZForeignKey(Function)
    unused = WZUnusedField()


class ResearchStructure(WZResearchModel):
    '''presumably, this determines which structures are required in order for the research item to become available. (For example, you'll need an HQ in order to be able to research a whole bunch of defenses.)'''
    load_from_first = True
    FILE_PATH = 'stats/research/%s/researchstruct.txt'

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s allow %s' % (self.research, self.structure)

    def get_structure_link(self):
        return get_component_link(self.structure.key, 'structure')

class ResearchStructure_Cam1(ResearchStructure):
    FILE_PATH = ResearchStructure.FILE_PATH % ('cam1')
    research = WZForeignKey(Research_Cam1)
    structure = WZForeignKey(Structure)
    unused = WZUnusedField()


class ResearchStructure_Cam2(ResearchStructure):
    FILE_PATH = ResearchStructure.FILE_PATH % ('cam2')
    research = WZForeignKey(Research_Cam2)
    structure = WZForeignKey(Structure)
    unused = WZUnusedField()


class ResearchStructure_Cam3(ResearchStructure):
    FILE_PATH = ResearchStructure.FILE_PATH % ('cam3')
    research = WZForeignKey(Research_Cam3)
    structure = WZForeignKey(Structure)
    unused = WZUnusedField()


class ResearchStructure_Multiplayer(ResearchStructure):
    FILE_PATH = ResearchStructure.FILE_PATH % ('Multiplayer')
    research = WZForeignKey(Research_Multiplayer)
    structure = WZForeignKey(Structure)
    unused = WZUnusedField()



class ResultComponent(WZResearchModel):
    ''' determines which components are made available upon researching the selected item.'''
    FILE_PATH = 'stats/research/%s/resultcomponent.txt'
    load_from_first = True

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s replaces %s(%s) for %s(%s)' % (self.research, self.component, self.type, self.replaced_component, self.replaced_type)

    def get_component_link(self):
        return get_component_link(self.component, self.type)

class ResultComponent_Cam1(ResultComponent):
    FILE_PATH = ResultComponent.FILE_PATH % ('cam1')
    research = WZForeignKey(Research_Cam1)
    component = WZCharField(help_text='Name for component')
    type = WZCharField(choices=COMPONENT_TYPE)
    replaced_component = WZCharField(help_text='Name for component')
    replaced_type = WZCharField(choices=COMPONENT_TYPE)
    unused = WZUnusedField()


class ResultComponent_Cam2(ResultComponent):
    FILE_PATH = ResultComponent.FILE_PATH % ('cam2')
    research = WZForeignKey(Research_Cam2)
    component = WZCharField(help_text='Name for component')
    type = WZCharField(choices=COMPONENT_TYPE)
    replaced_component = WZCharField(help_text='Name for component')
    replaced_type = WZCharField(choices=COMPONENT_TYPE)
    unused = WZUnusedField()


class ResultComponent_Cam3(ResultComponent):
    FILE_PATH = ResultComponent.FILE_PATH % ('cam3')
    research = WZForeignKey(Research_Cam3)
    component = WZCharField(help_text='Name for component')
    type = WZCharField(choices=COMPONENT_TYPE)
    replaced_component = WZCharField(help_text='Name for component')
    replaced_type = WZCharField(choices=COMPONENT_TYPE)
    unused = WZUnusedField()


class ResultComponent_Multiplayer(ResultComponent):
    FILE_PATH = ResultComponent.FILE_PATH % ('multiplayer')
    research = WZForeignKey(Research_Multiplayer)
    component = WZCharField(help_text='Name for component')
    type = WZCharField(choices=COMPONENT_TYPE)
    replaced_component = WZCharField(help_text='Name for component')
    replaced_type = WZCharField(choices=COMPONENT_TYPE)
    unused = WZUnusedField()



class ResearchObsoleteComponent(WZResearchModel):
    '''presumably, this determines which components(weapon or body) are made obsolete upon researching the selected item.'''
    load_from_first = True
    FILE_PATH = 'stats/research/%s/redcomponents.txt'

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s obsolete %s(%s)' % (self.research, self.component, self.type)

    def get_component_link(self):
        return get_component_link(self.component, self.type)


class ResearchObsoleteComponent_Cam1(ResearchObsoleteComponent):
    FILE_PATH = ResearchObsoleteComponent.FILE_PATH % ('cam1')
    research = WZForeignKey(Research_Cam1)
    component = WZCharField(help_text='Name for component')
    type = WZCharField(choices=COMPONENT_TYPE)
    unused = WZUnusedField()


class ResearchObsoleteComponent_Cam2(ResearchObsoleteComponent):
    FILE_PATH = ResearchObsoleteComponent.FILE_PATH % ('cam2')
    research = WZForeignKey(Research_Cam2)
    component = WZCharField(help_text='Name for component')
    type = WZCharField(choices=COMPONENT_TYPE)
    unused = WZUnusedField()


class ResearchObsoleteComponent_Cam3(ResearchObsoleteComponent):
    FILE_PATH = ResearchObsoleteComponent.FILE_PATH % ('cam3')
    research = WZForeignKey(Research_Cam3)
    component = WZCharField(help_text='Name for component')
    type = WZCharField(choices=COMPONENT_TYPE)
    unused = WZUnusedField()


class ResearchObsoleteComponent_Multiplayer(ResearchObsoleteComponent):
    FILE_PATH = ResearchObsoleteComponent.FILE_PATH % ('multiplayer')
    research = WZForeignKey(Research_Multiplayer)
    component = WZCharField(help_text='Name for component')
    type = WZCharField(choices=COMPONENT_TYPE)
    unused = WZUnusedField()

class ResearchObsoletStructure(WZResearchModel):
    '''presumably, this determines which structures are made obsolete upon researching the selected item.'''
    FILE_PATH = 'stats/research/%s/redstructure.txt'
    load_from_first = True

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s obsolete %s' % (self.research, self.structure)

    def get_structure_link(self):
        return get_component_link(self.structure.key, 'structure')

class ResearchObsoletStructure_Cam1(ResearchObsoletStructure):
    FILE_PATH = ResearchObsoletStructure.FILE_PATH % ('cam1')
    research = WZForeignKey(Research_Cam1)
    structure = WZForeignKey(Structure)
    unused = WZUnusedField()

class ResearchObsoletStructure_Cam2(ResearchObsoletStructure):
    FILE_PATH = ResearchObsoletStructure.FILE_PATH % ('cam2')
    research = WZForeignKey(Research_Cam2)
    structure = WZForeignKey(Structure)
    unused = WZUnusedField()

class ResearchObsoletStructure_Cam3(ResearchObsoletStructure):
    FILE_PATH = ResearchObsoletStructure.FILE_PATH % ('cam3')
    research = WZForeignKey(Research_Cam3)
    structure = WZForeignKey(Structure)
    unused = WZUnusedField()


class ResearchObsoletStructure_Multiplayer(ResearchObsoletStructure):
    FILE_PATH = ResearchObsoletStructure.FILE_PATH % ('multiplayer')
    research = WZForeignKey(Research_Multiplayer)
    structure = WZForeignKey(Structure)
    unused = WZUnusedField()


