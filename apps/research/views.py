from itertools import count

def get_node(research, relation_counter):

    bonuses = ['+ %s' %x.structure for x in research.researchstructure_set.all()]
    bonuses.extend(['+ %s' % x.function for x in research.researchfunctions_set.all()])
    bonuses.extend(['+ %s(%s)' % (x.component, x.type) for x in research.researchcomponent_set.all()])

    bonuses.extend(['- %s' % x.structure for x in research.researchobsoletstructure_set.all()])
    bonuses.extend(['- %s(%s)' % (x.component, x.type) for x in research.researchobsoletecomponent_set.all()])

    label = '|'.join((['%s' % research] + bonuses))
    res =  ['"node%s" [ label = "%s" ];' % (research.counter, label)]
    nodes = []
    for index, pre in enumerate(research.pre_requisites.all()):
        nodes.append('"node%s":f%s -> "node%s":f0 [id = %s ];' % (research.counter, index, pre.research.counter, relation_counter.next()))
    return res, nodes



def create_graviz_tree():
    relation_counter = count()
    from apps.research.models import Research_Cam1 as Research
    template = '''
    digraph g {
    graph [rankdir = "LR"];
    node [fontsize = "16" style=filled, fillcolor=yellow shape = "Mrecord"];
    edge [];
    %s
    }
    '''
    edges = []
    nodes = []
    for research in Research.objects.all():
        node, edge = get_node(research, relation_counter)
        nodes.extend(node)
        edges.extend(edge)

    res = template % '\n'.join(nodes + edges)

    with open('research.gv', 'w') as f:
        f.write(res)
