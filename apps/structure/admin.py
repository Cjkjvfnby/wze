from models import Structure, StructureDefence, StructureWeapon, BodyDefence, Feature
from apps.function.models import StructureFunction
from django.contrib import admin
from apps.admin.admin_models import register, WZInline, WZModelAdmin


class FunctsInline(WZInline):
    model = StructureFunction
    extra = 0
    max_num = 4

class WeaponsInline(WZInline):
    model = StructureWeapon
    extra = 0
    max_num = 4


class StructureDefenceAdmin(WZModelAdmin):
    model = StructureDefence


class BodyDefenceAdmin(WZModelAdmin):
    model = BodyDefence


class FeatureAdmin(WZModelAdmin):
    model = Feature

register(StructureDefenceAdmin)
register(BodyDefenceAdmin)
register(FeatureAdmin)


class StructureAdmin(WZModelAdmin):
    list_display = ('__str__', 'type', 'size', 'hp', 'repair_system')
    inlines = [FunctsInline, WeaponsInline]
    model = Structure

    fieldsets = (
        ('Stats', {
            'fields': (('power_to_build', 'build_points', 'hp', 'type',),)
        }),


        ('Sizes', {
            'fields': (('breadth', 'height', 'width'),)
        }),

        ('Defence', {
            'fields': ( ('armour_value', 'resistance', 'strength'),)
        }),


        ('Systems', {

            'fields': (( 'ecm', 'sensor',
                         'repair_system'),)

        }),

        ('Pies', {
            'classes': ('collapse',),
            'fields': (('gfx_file', 'base_IMD'),)

        }),


        )


register(StructureAdmin)