from apps.admin.fields import (WZCharField, WZNameField, WZPieField, WZPositiveIntegerField,
                               WZForeignKey, WZUnusedField, WZBooleanIntField)
from apps.admin.model import WZModel

from apps.admin.utils import get_file
from apps.weapon.models import Weapon, ECM, Sensor


class DefenceModifiers(WZModel):
    class Meta:
        abstract = True

    key = WZNameField()
    personal = WZPositiveIntegerField()
    tank = WZPositiveIntegerField()
    bunker_busters = WZPositiveIntegerField()
    artillery = WZPositiveIntegerField()
    flamer = WZPositiveIntegerField()
    aircraft = WZPositiveIntegerField()

    load_from_first = True
    NAMES = {
        'ANTI PERSONNEL': 'personal',
        'ANTI TANK': 'tank',
        'BUNKER BUSTER': 'bunker_busters',
        'ARTILLERY ROUND': 'artillery',
        'FLAMER': 'flamer',
        'ANTI AIRCRAFT': 'aircraft'
    }

    @classmethod
    def get_csv(cls):
        res = []
        NAMES_DICT = dict([(x, y) for y, x in cls.NAMES.iteritems()])
        for obj in cls.objects.all():
            for field in NAMES_DICT.keys():
                res.append(','.join([NAMES_DICT[field], obj.key, str(getattr(obj, field))]))
        return '\n'.join(res)


    @classmethod
    def load(cls):
        lines = get_file(cls)
        cls.objects.all().delete()
        lines.sort(key=lambda x: x[1])
        lines = zip(*[iter(lines)] * 6)
        res = []

        for line in lines:
            sd = cls()

            for defenses in line:
                defense, key, val = defenses
                sd.key = key
                setattr(sd, cls.NAMES[defense], val)
            res.append(sd)
        cls.bulk_save(res)

    def __unicode__(self):
        return self.key


class StructureDefence(DefenceModifiers):
    FILE_PATH = 'stats/structuremodifier.txt'


class BodyDefence(DefenceModifiers):
    FILE_PATH = 'stats/weaponmodifier.txt'


STRUCTURE_TYPE = [(x, x) for x in ['COMMAND RELAY',
                                   'CORNER WALL',
                                   'CYBORG FACTORY',
                                   'DEFENSE',
                                   'DEMOLISH',
                                   'FACTORY',
                                   'FACTORY MODULE',
                                   'HQ',
                                   'MISSILE SILO',
                                   'POWER GENERATOR',
                                   'POWER MODULE',
                                   'REARM PAD',
                                   'REPAIR FACILITY',
                                   'RESEARCH',
                                   'RESEARCH MODULE',
                                   'RESOURCE EXTRACTOR',
                                   'VTOL FACTORY',
                                   'WALL']]



class WeapornCountField(WZPositiveIntegerField):
    visible = False
    def funct(self, obj, x):
        return str(obj.structureweapon_set.count())

class FunctionCountField(WZPositiveIntegerField):
    visible = False
    def funct(self, obj, x):
        return str(obj.structurefunction_set.count())





class Structure(WZModel):
    FILE_PATH = 'stats/structures.txt'
    key = WZNameField()
    type = WZCharField(choices=STRUCTURE_TYPE)
    tech_level = WZUnusedField()
    strength = WZForeignKey(StructureDefence)
    terrain_type = WZUnusedField()
    width = WZPositiveIntegerField(
        help_text='Number of Y tiles used by this structure. Size on map = baseWidth * baseHeight')
    breadth = WZPositiveIntegerField(
        help_text='Number of X tiles used by this structure. Size on map = baseWidth * baseHeight ')
    foundation = WZUnusedField()
    build_points = WZPositiveIntegerField(
        help_text='The build points of a structure; affects time to build. Buildtime = buildpoints/truck buildpoints per second.')
    height = WZPositiveIntegerField(
        help_text='The height of the structure. Affects whether or not it can fire over walls.')
    armour_value = WZPositiveIntegerField(help_text='Armor against both kinetic and thermal weapons.')
    hp = WZPositiveIntegerField(help_text='HP of the structure.')
    repair_system = WZPositiveIntegerField(help_text='The internal name of repair turret. NULL if not repair facility.')
    power_to_build = WZPositiveIntegerField(help_text='Price of this structure.')
    dummyval1 = WZUnusedField()
    resistance = WZPositiveIntegerField(help_text='Resistance points to NEXUS convert/capture weapons.')
    dummyval2 = WZUnusedField()
    size_modifier = WZUnusedField()
    ecm = WZForeignKey(ECM, help_text='The internal name of ECM turret. NULL if not ECM tower.')
    sensor = WZForeignKey(Sensor,
        help_text='The internal name of sensor component. Affects visibility range; can be sensor turret.')
    weaporn_slots = WZPositiveIntegerField(
        help_text='The number of weapons that can be installed on this structure, current MAX is 4. Might be obsoleted by numWeaps.')
    gfx_file = WZPieField(help_text='The pie model name used by this structure.')
    base_IMD = WZPieField(help_text='The base pie/texture below the structure.')
    num_functions = FunctionCountField(default=0, limit=[0,4],
        help_text='The number of function that can be installed on this structure, current MAX is 4.')
    num_weapons = WeapornCountField(default=0, limit=[0,4],
        help_text='The number of weapons that can be installed on this structure, current MAX is 4.')


    def size(self):
        return '%s X %s X %s' % (self.width, self.breadth, self.height,)


class StructureWeapon(WZModel):
    FILE_PATH = 'stats/structureweapons.txt'
    load_from_first = True

    class Meta:
        ordering = ["structure"]

    structure = WZForeignKey(Structure)
    weapon = WZForeignKey(Weapon, related_name='weaporn1', blank=True, null=True)
    number = WZUnusedField(default=0)


    @classmethod
    def get_csv(cls):
        res = []
        for structure in Structure.objects.exclude(structureweapon=None):
            line = [structure.key]
            structire_weapon_set = structure.structureweapon_set.all()
            line.extend([wp.weapon.key for wp in structire_weapon_set])
            line.extend(['NULL'] * (5 - len(line)))
            line.append(str(structire_weapon_set[0].number))
            res.append(','.join(line))

        return '\n'.join(res)


    def __unicode__(self):
        return '%s of %s' % (self.weapon.get_name(), self.structure.get_name())

    @classmethod
    def load(cls):
        lines = get_file(cls)
        cls.objects.all().delete()
        res = []
        for line in lines:
            struct_key, wp1, wp2, wp3, wp4, number = line
            struct = Structure.objects.get(key=struct_key)
            res.extend([StructureWeapon(
                structure=struct,
                weapon=Weapon.objects.get(key=key),
                number=number) for key in (wp1, wp2, wp3, wp4) if key != 'NULL'])
        cls.bulk_save(res)


FEATURE_TYPE = [(x, x) for x in ['BOULDER',
                                 'BUILDING',
                                 'GENERIC ARTEFACT',
                                 'OIL DRUM',
                                 'OIL RESOURCE',
                                 'SKYSCRAPER',
                                 'TANK WRECK',
                                 'TREE',
                                 'VEHICLE']]


class Feature(WZModel):
    FILE_PATH = 'stats/features.txt'
    key = WZNameField()
    width = WZPositiveIntegerField(
        help_text='Number of Y tiles used by this structure. Size on map = baseWidth * baseHeight')
    breadth = WZPositiveIntegerField(
        help_text='Number of X tiles used by this structure. Size on map = baseWidth * baseHeight.')
    damageable = WZBooleanIntField(
        help_text="Whether or not the feature is damagable, if set to 0, the 'x' cursor will appear instead of the 'attack' cursor.")
    armor = WZPositiveIntegerField(help_text="Armour of the feature, similar to armour used on buildings.")
    hp = WZPositiveIntegerField(help_text='HP of the feature.')
    model = WZPieField(help_text="Name of feature graphics file.")
    type = WZCharField(choices=FEATURE_TYPE, help_text="Type of feature.")
    titledraw = WZUnusedField()
    allow_LOS = WZBooleanIntField(
        help_text='(LOS=Line Of Sight) Whether of not units and structures can see through them and shoot at objects behind them.')
    start_visible = WZBooleanIntField(
        help_text="Whether or not the feature is visible even if you haven't explored that area of terrain yet.")