from models import StructureFunction, Function
from django.contrib import admin
from apps.admin.admin_models import  register, WZModelAdmin


class FunctionAdmin(WZModelAdmin):
    model = Function


register(FunctionAdmin)