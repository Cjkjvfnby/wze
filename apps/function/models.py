from apps.admin.fields import WZCharField, WZNameField, WZPositiveIntegerField, WZForeignKey
from apps.structure.models import Structure

from apps.admin.model import WZModel


#NEXUSWall,NEXUS Wall1,81


FUNCTIONS = [(x, x) for x in (
    "Production", "Production Upgrade", "Research", "Research Upgrade", "Power Generator", 'Resource',
    'Repair Droid', 'Weapon Upgrade', 'Wall Function', 'Structure Upgrade', 'WallDefence Upgrade',
    'Power Upgrade', 'Repair Upgrade', 'VehicleRepair Upgrade', 'VehicleECM Upgrade', 'VehicleConst Upgrade',
    'VehicleBody Upgrade', 'VehicleSensor Upgrade', 'ReArm', 'ReArm Upgrade')]


class Function(WZModel):
    FILE_PATH = 'stats/functions.txt'
    load_from_first = True
    function = WZCharField(choices=FUNCTIONS)
    key = WZNameField()
    args = WZCharField(help_text='coma separated function arguments')

    class Meta:
        ordering = ["function"]


    @classmethod
    def from_csv(cls, line):
        line = [line[0], line[1], ','.join(line[2:])]
        return super(Function, cls).from_csv(line)


class StructureFunction(WZModel):
    FILE_PATH = 'stats/structurefunctions.txt'
    load_from_first = True
    object = WZForeignKey(Structure)
    function = WZForeignKey(Function)
    argument = WZPositiveIntegerField()

    class Meta:
        ordering = ["object"]


    def __unicode__(self):
        return self.function.key
