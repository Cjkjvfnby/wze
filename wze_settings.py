import os
from user_settings import PATH_TO_MOD

if not os.path.exists(PATH_TO_MOD):
    raise Exception('PATH_TO_MOD %s does not exists' % PATH_TO_MOD)
MOD_SOURCE = os.path.isdir(PATH_TO_MOD)

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'base.db',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
#    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'apps.mod',
    'apps.weapon',
    'apps.structure',
    'apps.function',
    'apps.body',
    'apps.templates',
    'apps.research',

    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    )

TEMPLATE_DIRS = ('templates',)