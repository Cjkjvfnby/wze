from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()
from django.conf import settings


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'wze.views.home', name='home'),
    # url(r'^wze/', include('wze.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^load/$', 'apps.mod.views.load', name="load"),
    url(r'^save/(?P<format>\w*|\\)$', 'apps.mod.views.save', name='save'),
    url(r'^tables/$', 'apps.mod.views.table_info', name='tables'),
    url(r'^researches/$', 'apps.mod.views.researches', name='researches'),
    url(r'^research_tree/$', 'apps.mod.views.save_graviz_tree'),
    url(r'^data_mining/$', 'apps.mod.views.data_mining'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(admin.site.urls)),
)
if settings.DEBUG:
   from django.views.static import serve
   _media_url = settings.MEDIA_URL
   if _media_url.startswith('/'):
       _media_url = _media_url[1:]
       urlpatterns += patterns('',
        (r'^%s(?P<path>.*)$' % _media_url,
        serve,
        {'document_root': settings.MEDIA_ROOT}))
   del(_media_url, serve)